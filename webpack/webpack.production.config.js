const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StatsPlugin = require('stats-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const distDir = `${__dirname}/../dist`;
const srcDir = `${__dirname}/../src`;

module.exports = [
    {
        name: 'client',
        target: 'web',
        entry: `${srcDir}/client.jsx`,
        output: {
            path: distDir,
            filename: 'client.js',
            publicPath: '/dist/',
        },
        resolve: {
            extensions: ['.js', '.jsx', '.jss'],
            alias: {
                AppRoot: `${srcDir}/app`,
                StylesRoot: `${srcDir}/styles`,
                config: `${srcDir}/config/config.prod.js`,
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx|jss)$/,
                    exclude: /(node_modules\/)/,
                    use: [
                        {
                            loader: 'babel-loader',
                        },
                        {
                            loader: 'shebang-loader',
                        }
                    ]
                },
                {
                    test: /\.pcss$/,
                    exclude: /(node_modules\/)/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    modules: true,
                                    importLoaders: 1,
                                    localIdentName: '[hash:base64:5]',
                                    sourceMap: false,
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    config: {
                                        path: `${__dirname}/../postcss/postcss.config.js`,
                                    }
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[hash:base64:5].[ext]',
                        publicPath: 'font/',
                        outputPath: 'font/'
                    }
                },
                {
                    test: /\.(jpg|png)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[hash:base64:5].[ext]',
                        publicPath: 'img/',
                        outputPath: 'img/'
                    }
                }
            ],
        },
        plugins: [
            new CleanWebpackPlugin(distDir),
            new ExtractTextPlugin({
                filename: 'styles.css',
                allChunks: true
            }),
            new OptimizeCssAssetsPlugin({
                cssProcessorOptions: {discardComments: {removeAll: true}}
            }),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: '"production"'
                }
            }),
            new UglifyJsPlugin({
                //exclude: `${srcDir}/app/public/mqtt.jsx`,
                uglifyOptions: {
                    compress: {
                        warnings: false,
                        drop_console: true,
                        drop_debugger: true
                    },
                    parse: {
                        shebang: true,
                    },
                    output: {
                        comments: false,
                        beautify: false,
                    }
                }
            }),
            new webpack.optimize.OccurrenceOrderPlugin(),
        ]
    },
    {
        name: 'server',
        target: 'node',
        entry: `${srcDir}/server.jsx`,
        output: {
            path: distDir,
            filename: 'server.js',
            libraryTarget: 'commonjs2',
            publicPath: '/dist/',
        },
        resolve: {
            extensions: ['.js', '.jsx', '.jss'],
            alias: {
                AppRoot: `${srcDir}/app`,
                StylesRoot: `${srcDir}/styles`,
                config: `${srcDir}/config/config.prod.js`,
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx|jss)$/,
                    exclude: /(node_modules\/)(?!mqtt)/,
                    use: [
                        {
                            loader: 'babel-loader',
                        },
                        {
                            loader: 'shebang-loader',
                        }
                    ]
                },
                {
                    test: /\.pcss$/,
                    exclude: /(node_modules\/)/,
                    use: [
                        {
                            loader: 'isomorphic-style-loader',
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                importLoaders: 1,
                                localIdentName: '[hash:base64:5]',
                                sourceMap: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: `${__dirname}/../postcss/postcss.config.js`,
                                }
                            }
                        }

                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[hash:base64:5].[ext]',
                        publicPath: 'font/',
                        outputPath: 'font/'
                    }
                },
                {
                    test: /\.(jpg|png)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[hash:base64:5].[ext]',
                        publicPath: 'img/',
                        outputPath: 'img/'
                    }
                }
            ],
        },
        plugins: [
            new StatsPlugin('stats.json', {
                chunkModules: true,
                modules: true,
                chunks: true,
                exclude: [/node_modules[\\\/]react/],
            }),
        ]
    }
];
