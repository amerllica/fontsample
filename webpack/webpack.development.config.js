const ExtractTextPlugin = require('extract-text-webpack-plugin');

const distDir = `${__dirname}/../dist`;
const srcDir = `${__dirname}/../src`;

module.exports = [
    {
        name: 'client',
        target: 'web',
        entry: `${srcDir}/client.jsx`,
        output: {
            path: distDir,
            filename: 'client.js',
            publicPath: '/dist/',
        },
        resolve: {
            extensions: ['.js', '.jsx', '.jss'],
            alias: {
                AppRoot: `${srcDir}/app`,
                StylesRoot: `${srcDir}/styles`,
                config: `${srcDir}/config/config.dev.js`,
            }
        },
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.(js|jsx|jss)$/,
                    exclude: /(node_modules\/)(?!mqtt)/,
                    use: [
                        {
                            loader: 'babel-loader',
                        },
                        {
                            loader: 'shebang-loader',
                        }
                    ]
                },
                {
                    test: /\.pcss$/,
                    exclude: /(node_modules\/)/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    modules: true,
                                    importLoaders: 1,
                                    localIdentName: '[local]',
                                    sourceMap: true,
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    config: {
                                        path: `${__dirname}/../postcss/postcss.config.js`,
                                    }
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[name].[ext]',
                        publicPath: 'font/',
                        outputPath: 'font/'
                    }
                },
                {
                    test: /\.(jpg|png)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[name].[ext]',
                        publicPath: 'img/',
                        outputPath: 'img/'
                    }
                }
            ],
        },
        plugins: [
            new ExtractTextPlugin({
                filename: 'styles.css',
                allChunks: true
            })
        ]
    },
    {
        name: 'server',
        target: 'node',
        entry: `${srcDir}/server.jsx`,
        output: {
            path: distDir,
            filename: 'server.js',
            libraryTarget: 'commonjs2',
            publicPath: '/dist/',
        },
        resolve: {
            extensions: ['.js', '.jsx', '.jss'],
            alias: {
                AppRoot: `${srcDir}/app`,
                StylesRoot: `${srcDir}/styles`,
                config: `${srcDir}/config/config.dev.js`,
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx|jss)$/,
                    exclude: /(node_modules\/)(?!mqtt)/,
                    use: [
                        {
                            loader: 'babel-loader',
                        },
                        {
                            loader: 'shebang-loader',
                        }
                    ]
                },
                {
                    test: /\.pcss$/,
                    exclude: /(node_modules\/)/,
                    use: [
                        {
                            loader: 'isomorphic-style-loader',
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                importLoaders: 1,
                                localIdentName: '[local]',
                                sourceMap: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: `${__dirname}/../postcss/postcss.config.js`,
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[name].[ext]',
                        publicPath: 'font/',
                        outputPath: 'font/'
                    }
                },
                {
                    test: /\.(jpg|png)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: '[name].[ext]',
                        publicPath: 'img/',
                        outputPath: 'img/'
                    }
                }
            ],
        },
    }
];
