import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Login from 'AppRoot/pages/login/login';

export default (props) =>
    <Switch>
        <Route exact path='/login' render={() => <Login loginHandler={props.loginHandler}/>}/>
        <Redirect from='/**/' to='/login'/>
    </Switch>
