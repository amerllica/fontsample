import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Payment from 'AppRoot/pages/payment/payment';

export default (props) => (
    <Switch>
        <Route exact path='/' render={() => <Redirect to='/payment'/>}/>
        <Route exact path='/payment' render={() => <Payment fetchManager={props.fetchManager}/>}/>
        <Redirect from='/login' to={`${/\/login/.test(props.firstRoute) ? '/payment' : props.firstRoute}`}/>
        <Redirect from='/**/' to='/payment'/>
    </Switch>
);
