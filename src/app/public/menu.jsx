import React from 'react';
import {NavLink} from 'react-router-dom';
import styles from 'StylesRoot/styles.pcss';

export default () => (
    <nav className={styles.menu}>
        <NavLink activeClassName={styles.active} to={'/payment'}>صفحه اصلی</NavLink>
        <NavLink activeClassName={styles.active} to={'/contact'}>تماس با ما</NavLink>
    </nav>
);
