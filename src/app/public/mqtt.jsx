import React, {Component} from 'react';
import config from 'config';
//import styles from 'StylesRoot/styles.pcss';
import {connect} from 'mqtt/dist/mqtt';

const website = config.website;

const mqttConnectionUrl = `${website}/notify-service/api/v1/mqtt/connection-info`;

let client;

export default class Mqtt extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /*mqttConnect: false,*/
            mqttConnectionInfo: null,
        };

        ({
            mqttMessageHandler: this.mqttMessageHandler,
            mqttStateHandler: this.mqttStateHandler,
            fetchManager: this.fetchManager,
        } = props);
    }

    componentDidMount = () => {

        this.getMqttConnectionInfo();
    };

    componentWillUnmount = () => {

        if (client) {
            client.end();
            this.mqttStateHandler(false);
        }
    };

    subscribeMqttTopic = () => {

        const {
            mqttConnectionInfo: {
                brokers,
                topic,
                username,
                password,
            } = {},
        } = this.state;

        const url = /^https.+/.test(window.location.protocol)
            ? brokers.filter(item => /^mqtts:.+/.test(item))[0]
            : brokers.filter(item => /^mqtt:.+/.test(item))[0];

        client = connect(url, {
            username: username,
            password: password,
            clientId: `browser/${JSON.parse(localStorage.getItem('accessToken')).ud}`,
            connectTimeout: 1000
        });

        client.on('connect', () => {

            client.subscribe(topic);
            this.mqttStateHandler(true);
            /*this.setState({
                mqttConnect: true,
            });*/
        });

        client.on('offline', () => {

            this.mqttStateHandler(false);
            /*this.setState({
                mqttConnect: false,
            });*/
        });

        client.on('error', (e) => {

            console.log(`MQTT Error:`, e);
        });

        client.on('message', (topic, message, packet) => {
            this.mqttMessageHandler(JSON.parse(message.toString()));
        });

        /*client.on('packetsend', (e) => {
            console.log(`packet is sent: `, e);
        });

        client.on('packetreceive', (e) => {
            console.log(`packet is arrived: `, e);
        });*/
    };

    getMqttConnectionInfo = async () => {

        const mqttConnectionInfo = await this.fetchManager({url: mqttConnectionUrl, method: `GET`});

        this.setState({
            mqttConnectionInfo
        });

        if (this.state.mqttConnectionInfo) {
            this.subscribeMqttTopic();
        }
    };

    render = () => null;
}
