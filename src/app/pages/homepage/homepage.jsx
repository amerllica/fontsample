import React from 'react';
import Helmet from 'react-helmet';
import styles from 'StylesRoot/styles.pcss';

export default () => (
    <div className={styles.homepage}>
        <Helmet title='صفحه اصلی'/>
        <h1>خانه</h1>
    </div>
);
