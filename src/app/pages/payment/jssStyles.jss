const variables = {
    focused: '#ffffff',
    green: '#00897b',
    lightGreen: '#00a195',
    formControlBackground: '#008e80',

};

module.exports = {
    exitButton: {
        color: '#fff',
        marginRight: 'auto'
    },
    formControl: {
        background: variables.formControlBackground,
        borderRadius: '3px',
        marginBottom: '20px'
    },
    input: {
        color: variables.focused,
        '&:before': {
            borderColor: `${variables.lightGreen} !important`,
            borderRadius: '0 0 3px 3px',
        },
        '&:after': {
            borderColor: variables.focused,
            borderRadius: '0 0 3px 3px',
        }
    },
    inputLabel: {
        left: 'initial',
        right: '10px',
        fontSize: '15px',
        color: variables.lightGreen,
        top: '-2px',
    },
    inputLabelShrink: {
        top: '5px',
        color: `${variables.focused} !important`,
        zIndex: 1,
    },
    inputAdornment: {
        marginLeft: 0,
        transform: 'translateY(-7px)',
    },
    inputAdornmentIconButton: {
        color: variables.lightGreen
    },
    submitButton: {
        marginTop: '15px',
        marginBottom: '10px',
        background: '#000',
        color: variables.focused,
        '&:hover': {
            background: 'rgba(0,0,0,.75)'
        }
    },
    formHelperText: {
        color: 'rgba(255,255,255,.3)',
        marginTop: 0,
        paddingTop: '8px',
        background: variables.green,
        textAlign: 'right',
        fontSize: '10px',
    },
    cellStyle: {
        borderBottom: 'none',
        textAlign: 'right',
        padding: '15px 25px',
    },
    headerStyle: {
        background: '#c7c7c7',
        color: '#fff',
    },
    refundButton: {
        fontSize: '13px',
        padding: '5px 10px',
        background: '#b00020',
        color: '#fff',

        '&:hover': {
            background: '#ca0026',
        }
    }
};
