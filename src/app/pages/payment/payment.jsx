import React, {Component} from 'react';
import Helmet from 'react-helmet';
import styles from 'StylesRoot/styles.pcss';
import config from 'config';
import Mqtt from 'AppRoot/public/mqtt';
import {fromBase64} from 'bytebuffer/dist/bytebuffer';
import PersianDate from 'persian-date';
import PersianJs from 'persianjs';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import HighlightOff from '@material-ui/icons/HighlightOff';
import CallMissedOutgoing from '@material-ui/icons/CallMissedOutgoing';
import Done from '@material-ui/icons/Done';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import SignalWifiOff from '@material-ui/icons/SignalWifiOff';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import jssStyles from './jssStyles.jss';


const website = config.website;

const personPaymentUrl = `${website}/core-service/api/v1/member/payment-tokens`,
    paymentUrl = `${website}/core-service/api/v1/member/payments`,
    paymentsListUrl = `${website}/core-service/api/v1/member/transactions`;

const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

class TablePaginationActions extends Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const {classes, count, page, rowsPerPage, theme} = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    <LastPageIcon/>
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    <KeyboardArrowRight/>
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    <KeyboardArrowLeft/>
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    <FirstPageIcon/>
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {withTheme: true})(
    TablePaginationActions,
);

/*let counter = 0;*/

/*function createData(name, calories, fat) {
    counter += 1;
    return {id: counter, name, calories, fat};
}*/

let scannedPattern = '';

class Payment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            paymentState: 'wait', // wait, amount
            paymentAmount: '',
            accountId: '',
            paymentToken: '',
            offlinePayment: true,
            offlinePaymentToken: '',
            wannaSendPayment: false,
            paymentsList: {},
            wannaGetPaymentList: false,
            paymentsListSize: 6,
            paymentsListPage: 0,
            mqttMessage: null,
            mqttState: false,
            amountException: false,
            amountExceptionText: '',
            fallInException: false,
            fallInExceptionText: '',
        };

        this.fetchManager = this.props.fetchManager;
    }

    componentDidMount = () => {

        this.getPaymentsList();

        document.getElementById('payment-input').focus();
    };

    componentDidUpdate = () => {

        this.inputAutoFocus();

        if (this.state.wannaGetPaymentList) {

            this.getPaymentsList();

            this.setState({
                wannaGetPaymentList: false,
            });
        }

        if (this.state.mqttMessage != null && /VERIFIED/i.test(this.state.mqttMessage.type) && this.state.wannaSendPayment) {

            this.sendPayment(this.state.mqttMessage.token);
        }

        if (this.state.mqttMessage != null && /PAYMENT_TOKEN_CANCELLED/i.test(this.state.mqttMessage.type) && this.state.paymentState === 'amount') {

            this.setState({
                fallInException: true,
                fallInExceptionText: `پرداخت توسط مشتری لغو شد`,
            });

            this.handleClickResetPayment();
        }

        if (this.state.amountException) {

            this.fadeOutEffect(document.getElementById('amount-exception'), 'amountException', 75);
        }

        if (this.state.fallInException) {

            this.fadeOutEffect(document.getElementById('fall-in-exception'), 'fallInException', 75);
        }
    };

    fadeOutEffect = (fadeTarget, stateTarget, time = 25) => {
        if (fadeTarget == null) return false;

        const stateToFasle = {};
        stateToFasle[stateTarget] = false;
        const fadeEffect = setInterval(() => {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity < 0.05) {
                clearInterval(fadeEffect);
                this.setState(stateToFasle);
            } else {
                fadeTarget.style.opacity -= 0.01;
            }
        }, time);
    };

    mqttMessageHandler = mqttMessage => this.setState({
        mqttMessage,
    });

    mqttStateHandler = mqttState => this.setState({
        mqttState,
    });

    handleChangePage = (event, page) => {
        this.setState({
            paymentsListPage: page,
            wannaGetPaymentList: true,
        });
    };

    handleChangeRowsPerPage = e => {
        this.setState({
            paymentsListSize: e.target.value,
            wannaGetPaymentList: true,
            paymentsListPage: 0,
        });
    };

    handleClickResetPayment = () => {

        scannedPattern = '';

        this.setState({
            paymentState: 'wait',
            paymentAmount: '',
            accountId: '',
            inputPattern: '',
            paymentToken: '',
            offlinePayment: true,
            offlinePaymentToken: '',
            mqttMessage: null,
        })
    };

    handleClickResetAmount = () => this.setState({
        paymentAmount: '',
    });

    inputAutoFocus = () => {

        const paymentInput = document.getElementById('payment-input');

        if (paymentInput) {

            window.onfocus = () => paymentInput.focus();

            document.getElementById('root').onclick = (e) => {

                if (e.target.id !== 'amount-input') {

                    paymentInput.focus();
                }
            };
        }
    };

    getPaymentsList = async (size = this.state.paymentsListSize, page = this.state.paymentsListPage) => {

        const result = await this.fetchManager({
            url: `${paymentsListUrl}?size=${size}&page=${page}`,
            method: `GET`,
        });

        //console.log(result);

        this.setState({
            paymentsList: result,
        });
    };

    paymentTypeDetector = (scannedPattern) => {

        if (/@$/.test(scannedPattern)) {

            scannedPattern = scannedPattern.slice(0, -1);

            this.setState({
                paymentState: 'amount',
            });

            if (scannedPattern.includes('scann')) {

                this.setState({
                    offlinePayment: true,
                    /*accountId: scannedPattern === '' ? '' : this.state.accountId,*/
                });

                const match = scannedPattern.match(/pe\/(.*)/);
                const pattern = match ? match[1] : '';

                if (pattern.length === 128) {

                    const
                        bb = fromBase64(pattern),
                        accountId = bb.readLong().toString(),
                        paymentAmount = bb.readLong().toString();

                    if (parseInt(paymentAmount) === 0) {

                        this.setState({
                            offlinePayment: false,
                            accountId,
                        });

                    } else {

                        this.setState({
                            offlinePayment: true,
                            offlinePaymentToken: pattern,
                            accountId,
                            paymentAmount,
                        });
                    }

                } else {

                    this.handleClickResetPayment();
                }

            } else {

                if (scannedPattern.length > 19) {

                    scannedPattern = scannedPattern.substr(scannedPattern.length - 19);
                }

                this.setState({
                    offlinePayment: false,
                    accountId: scannedPattern,
                });
            }
        }
    };

    refundPayment = async (id) => {
        //console.log(id);

        const result = await this.fetchManager({
            url: `${paymentUrl}/${id}`,
            method: `DELETE`,
        });

        if (result.ok) {

            this.setState({
                wannaGetPaymentList: true,
            });
        }
    };

    sendPayment = async (token) => {

        this.setState({
            wannaSendPayment: false,
        });

        const result = await this.fetchManager({
            url: paymentUrl,
            method: `POST`,
            body: {
                token,
            }
        });

        if (result.ok) {

            const res = await result.json();

            //console.log(`Payment Sent Successfully: `, res);

            this.setState({
                paymentAmount: '',
                accountId: '',
                inputPattern: '',
                paymentToken: '',
                offlinePayment: true,
                wannaGetPaymentList: true,
            });

            this.handleClickResetPayment();

        } else {
            console.log(await result.json());
            throw new Error(`sendPayment of Payment has error`);
        }
    };

    getPaymentToken = async () => {

        this.setState({
            amountException: false,
        });

        let result;

        if (this.state.offlinePayment) {

            result = await this.fetchManager({
                url: paymentUrl,
                method: `POST`,
                body: {
                    token: this.state.offlinePaymentToken,
                }
            });

        } else {

            result = await this.fetchManager({
                url: personPaymentUrl,
                method: `POST`,
                body: {
                    amount: this.state.paymentAmount,
                    accountId: this.state.accountId
                }
            });
        }

        if (result.ok) {

            const res = await result.json();

            //console.log(`successful paymentToken: `, res);
            this.setState({
                paymentToken: res,
                wannaSendPayment: true,
                wannaGetPaymentList: this.state.offlinePayment
            });


        } else if (result.status === 400) {

            const res = await result.json();

            console.log(`failed paymentToken: `, res);

            if (res.developerMessage === 'TransactionMinAmountException') {

                console.log(`Less than ${config.minAmount} IRR is not allowed`);

                this.setState({
                    amountException: true,
                    amountExceptionText: `کمتر از مقدار ${PersianJs(config.minAmount).englishNumber()} ریال مجاز نمی‌باشد`,
                });

            } else if (res.developerMessage === 'TransactionMaxAmountException') {

                console.log(`More than ${config.maxAmount} IRR is not allowed`);

                this.setState({
                    amountException: true,
                    amountExceptionText: `بیشتر از مقدار ${PersianJs(config.maxAmount).englishNumber()} ریال مجاز نمی‌باشد`,
                });
            }


        } else {
            console.log(await result.json());
            throw new Error(`getPaymentToken of Payment has error`);
        }

    };

    forcedToEnglish = (char) => {

        const code = char.nativeEvent.code;

        switch (true) {

            case code.startsWith(`Key`):

                if (char.shiftKey) {

                    return code[3].toUpperCase();

                } else {

                    return code[3].toLowerCase();
                }

            case code.startsWith(`Digit`):

                return code[5];

            default:

                return char.key.length === 1
                    ? char.key
                    : (
                        char.key === 'Enter'
                            ? '@'
                            : ''
                    );
        }
    };

    switchPaymentRender = () => {

        if (this.state.mqttState) {

            switch (this.state.paymentState) {
                case 'wait':
                    return this.renderPaymentWait();
                case 'amount':
                    return this.renderPaymentAmount();
                default:
                    return this.renderPaymentWait();
            }

        } else {

            return this.renderMqttDisconnect();
        }
    };

    renderPaymentsList = () => {

        const {
            classes,
        } = this.props;

        const paymentsList = typeof this.state.paymentsList.content !== 'undefined'
            ? [...this.state.paymentsList.content]
            : [];

        const refundTimeLimitation = 300000;

        const refundButton = ({id, createdDate, type, refunded}) => {

            const canStillRefund = (createdDate + refundTimeLimitation) - Date.now();

            return (
                !refunded && canStillRefund > 0
                    ? (
                        <Button variant='contained'
                                classes={{root: classes.refundButton}}
                                onClick={() => this.refundPayment(id)}
                                ref={() => setTimeout(() => this.forceUpdate(), refundTimeLimitation)}
                        >
                            <CallMissedOutgoing style={{paddingLeft: '3px'}}/>
                            <span>بازگشت وجه</span>
                        </Button>
                    )
                    : (
                        <span className={styles['payment__situation-text']}>
                            <span className={styles['payment__situation-text--mark']}>
                                {refunded ? <CallMissedOutgoing/> : <Done/>}
                            </span>
                            <span>
                                {refunded ? `بازگشت وجه` : `پرداخت موفق`}
                            </span>
                        </span>
                    )
            );
        };

        const {
            paymentsListSize,
            paymentsListPage,
            paymentsList: paymentsListData
        } = this.state;

        const emptyRows = (6 - paymentsList.length) * 66;

        return (
            paymentsList.length
                ? (
                    <Table>
                        <TableHead>
                            <TableRow classes={{root: classes.headerStyle}}>
                                <TableCell classes={{root: classes.cellStyle}}>مشخصات کاربر</TableCell>
                                <TableCell numeric classes={{root: classes.cellStyle}}>شماره پیگیری</TableCell>
                                <TableCell classes={{root: classes.cellStyle}}>تاریخ</TableCell>
                                <TableCell classes={{root: classes.cellStyle}}>وضعیت</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                paymentsList.map((payment, index) => (
                                    payment.type !== 'PAYMENT_REFUND'
                                        ? (
                                            <TableRow key={payment.id} className={index === 0 ? styles['first-row'] : ''}>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    {PersianJs(payment.fromAccountId).englishNumber().toString()}
                                                </TableCell>
                                                <TableCell numeric classes={{root: classes.cellStyle}}>
                                                    {PersianJs(payment.traceNumber).englishNumber().toString()}
                                                </TableCell>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    {new PersianDate(payment.createdDate).toCalendar('persian').format()}
                                                </TableCell>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    {refundButton(payment)}
                                                </TableCell>
                                            </TableRow>
                                        )
                                        : (
                                            <TableRow key={payment.id} className={index === 0 ? styles['first-row'] : ''}>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    {PersianJs(payment.fromAccountId).englishNumber().toString()}
                                                </TableCell>
                                                <TableCell numeric classes={{root: classes.cellStyle}}>
                                                    {PersianJs(payment.traceNumber).englishNumber().toString()}
                                                </TableCell>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    {new PersianDate(payment.createdDate).toCalendar('persian').format()}
                                                </TableCell>
                                                <TableCell classes={{root: classes.cellStyle}}>
                                                    <span className={styles['payment__situation-text']}>
                                                        <span className={styles['payment__situation-text--mark']}>
                                                            <Done/>
                                                        </span>
                                                        <span>
                                                            بازگشت موفق
                                                        </span>
                                                    </span>
                                                </TableCell>
                                            </TableRow>
                                        )
                                ))
                            }
                            {emptyRows > 0 && (
                                <TableRow style={{height: emptyRows}}>
                                    <TableCell colSpan={4}/>
                                </TableRow>
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <TablePagination
                                        component={props => props.children}
                                        count={paymentsListData.totalElements} page={paymentsListPage}
                                        rowsPerPage={paymentsListSize} rowsPerPageOptions={[1, 2, 3, 4, 5, 6]}
                                        labelRowsPerPage={`تعداد سطر در هر صفحه`}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActionsWrapped}
                                        labelDisplayedRows={({from, to, count}) => `${PersianJs(from).englishNumber()}-${PersianJs(to).englishNumber()} از ${PersianJs(count).englishNumber()}`}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableFooter>
                    </Table>
                )
                : (
                    <div className={styles['payment__form__list--empty-list']}/>
                )
        );
    };

    renderPaymentWait = () => {
        return (
            <div className={styles['payment-wait']}>
                <svg className={styles['wait-for-scann-logo']}>
                    <path
                        d="M106.192 114.979H88.873V104.53h15.676V88.851h10.473v17.309c0 4.869-3.953 8.819-8.83 8.819zM-.012 52.273h115.034v10.453H-.012V52.273zM104.549 10.47H88.873V.016h17.319c4.877 0 8.83 3.953 8.83 8.819v17.316h-10.473V10.47zM10.427 26.151H-.012V8.835c0-4.866 3.936-8.819 8.796-8.819H26.12V10.47H10.427v15.681zm0 78.379H26.12v10.449H8.784c-4.86 0-8.796-3.95-8.796-8.819V88.851h10.439v15.679z"/>
                </svg>
                <div className={styles.first}>
                    در انتظار پرداخت
                </div>
                <div className={styles.second}>
                    برای شروع کد کاربر را اسکن کنید
                </div>
            </div>
        );
    };

    renderPaymentAmount = () => {

        const {
            classes,
        } = this.props;

        return (
            <div className={styles['payment-amount']}>
                <div className={styles.first}>پرداخت جدید</div>
                <div className={styles.second}>
                    {!this.state.offlinePayment && `مبلغ را وارد کنید`}
                </div>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="name-simple" className={classes.inputLabel}
                                classes={{shrink: classes.inputLabelShrink}}>ریال</InputLabel>
                    <Input type='number'
                           id='amount-input'
                           ref={amountInput => amountInput ? document.getElementById(amountInput.props.id).focus() : null}
                           classes={{root: classes.input}}
                           value={this.state.paymentAmount}
                           onChange={(e) => this.setState({
                               paymentAmount: e.target.value,
                           })}
                           onKeyDown={e => {
                               if (e.key === 'Enter') {
                                   document.getElementById('payment-action').click();
                               }
                           }}
                           disabled={this.state.offlinePayment}
                           endAdornment={
                               <InputAdornment position="end" classes={{root: classes.inputAdornment}}
                                               disabled={this.state.offlinePayment}>
                                   <IconButton
                                       classes={{root: classes.inputAdornmentIconButton}}
                                       onClick={this.handleClickResetAmount}
                                   >
                                       <HighlightOff/>
                                   </IconButton>
                               </InputAdornment>
                           }
                    />
                    <FormHelperText classes={{root: classes.formHelperText}}>
                        {`حداقل مبلغ ${PersianJs(config.minAmount).englishNumber()} ریال `}
                        {` و `}
                        {`حداکثر مبلغ ${PersianJs(config.maxAmount).englishNumber()} ریال می‌باشد`}
                    </FormHelperText>
                </FormControl>
                <Button id='payment-action' classes={{root: classes.submitButton}} onClick={this.getPaymentToken}>
                    انجام پرداخت
                </Button>
                <Button onClick={this.handleClickResetPayment}>
                    انصراف
                </Button>
            </div>
        );
    };

    renderMqttDisconnect = () => {
        return (
            <div className={styles['mqtt-disconnect']}>
                <SignalWifiOff/>
                <div className={styles.first}>به اینترنت متصل نیستید</div>
                <div className={styles.second}>در حالت آفلاین امکان انجام پرداخت وجود ندارد</div>
            </div>
        );
    };

    renderAmountException = () => {
        return (
            this.state.amountException && (
                <div className={styles['fall-in-exception']} id='amount-exception'>
                    {this.state.amountExceptionText}
                    <IconButton style={{color: '#fff'}} onClick={() => this.setState({
                        amountException: false,
                    })}
                    >
                        <HighlightOff/>
                    </IconButton>
                </div>
            )
        );
    };

    renderFallInException = () => {
        return (
            this.state.fallInException && (
                <div className={styles['fall-in-exception']} id='fall-in-exception'>
                    {this.state.fallInExceptionText}
                </div>
            )
        );
    };

    render = () => (
        <div className={styles.payment}>
            <Helmet title='پرداخت'/>
            <Mqtt
                fetchManager={this.fetchManager}
                mqttMessageHandler={this.mqttMessageHandler}
                mqttStateHandler={this.mqttStateHandler}
            />
            {this.renderAmountException()}
            {this.renderFallInException()}
            <div className={`${styles.payment__form} ${this.state.mqttState ? '' : styles['mqtt-is-disconnected']}`}>
                <div className={styles.payment__form__list}>
                    <div className={styles['payment__form__list--title']}>
                        <span>تراکنش‌ها</span>
                    </div>
                    {this.renderPaymentsList()}
                </div>
                <div className={styles.payment__form__input}>
                    <input type='text' id='payment-input'
                           onKeyPress={(e) => {

                               scannedPattern = document.getElementById('payment-input').value + this.forcedToEnglish(e);

                               if (e.key === 'Enter') {

                                   document.getElementById('payment-input').value = '';

                                   this.paymentTypeDetector(scannedPattern);
                               }
                           }}
                    />
                    {this.switchPaymentRender()}
                </div>
            </div>
        </div>
    );
}

Payment.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(Payment);
