import React, {Component} from 'react';
import Helmet from 'react-helmet';
import AppRoutes from 'AppRoot/routes/appRoutes';
import LoginRoutes from 'AppRoot/routes/loginRoutes';
import styles from 'StylesRoot/styles.pcss';
import config from 'config';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const website = config.website;

const userInfoUrl = `${website}/core-service/api/userinfo`,
    loginUrl = `${website}/core-service/api/oauth/token`,
    logoutUrl = `${website}/core-service/api/v1/user/logout`;

const variables = {
    focused: '#ffffff',
    green: '#00897b',
    lightGreen: '#00a195',
    formControlBackground: '#008e80',

};

const jssStyles = {
    exitButton: {
        color: '#fff',
        marginRight: 'auto'
    }
};

class App extends Component {

    constructor() {
        super();

        this.state = {
            login: false,
            overlay: false,
            overlayChild: null,
        };

    }

    componentWillMount = () => {

        if (this.props.pathname && !/\/favicon/.test(this.props.pathname)) {

            this.firstRoute = this.props.pathname;
        }

        if (typeof window !== 'undefined') {

            this.firstRoute = window.location.pathname;
        }
    };

    componentDidMount = () => {

        this.loginStateHandler(!!JSON.parse(localStorage.getItem('accessToken')));
    };

    authenticationCheck = async () => {

        let body = new FormData();
        body.append('refresh_token', localStorage.getItem('refreshToken'));
        body.append('grant_type', 'refresh_token');
        body.append('deviceId', '888');

        let result = await fetch(`${loginUrl}`, {
            method: `POST`,
            headers: {
                'authorization': config.basicAuth
            },
            body: body
        });

        return await result.json();
    };

    fetchManager = async ({url, method, body}) => {

        this.overlayStateHandler(true, this.renderIndeterminateLoader());

        const authorization = `Bearer ${JSON.parse(localStorage.getItem('accessToken')).accessToken}`;

        let result;

        switch (method) {

            case `DELETE`:

                if (localStorage.getItem('accessToken') == null) {

                    this.loginStateHandler(false);

                } else {

                    result = await fetch(url, {
                        method,
                        headers: {
                            authorization,
                        }
                    });

                    if (result.status === 401) {

                        console.log(`1st ajax failure: `, url, await result.json());

                        const res = await this.authenticationCheck();

                        if (res.error) {

                            console.log(`2nd ajax failure: `, url, res);

                            this.loginStateHandler(false);

                        } else {

                            this.writeTokensLocalStorage(res);

                            this.overlayStateHandler(false, null);

                            return await fetch(url, {
                                method,
                                headers: {
                                    'authorization': `Bearer ${res.access_token}`,
                                }
                            });
                        }

                    } else if (result.status === 400) {

                        console.log(await result.json());

                    } else {

                        this.overlayStateHandler(false, null);

                        return result;
                    }
                }

                break;

            case `GET`:

                if (localStorage.getItem('accessToken') == null) {

                    this.loginStateHandler(false);

                } else {

                    result = await (await fetch(url, {
                        method,
                        headers: {
                            authorization
                        }
                    })).json();

                    if (result.error) {

                        console.log(`1st ajax failure: `, url, result);

                        const res = await this.authenticationCheck();

                        if (res.access_token) {

                            this.writeTokensLocalStorage(res);

                            this.overlayStateHandler(false, null);

                            return await (await fetch(url, {
                                method,
                                headers: {
                                    'authorization': `Bearer ${res.access_token}`,
                                }
                            })).json();

                        } else {

                            console.log(`2nd ajax failure: `, url, res);

                            this.loginStateHandler(false);
                        }
                    } else {

                        this.overlayStateHandler(false, null);

                        return result;
                    }
                }

                break;

            case `POST`:

                if (localStorage.getItem('accessToken') == null) {

                    this.loginStateHandler(false);

                } else {

                    result = await fetch(url, {
                        method,
                        headers: {
                            authorization,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(body)
                    });

                    if (result.status === 401) {

                        console.log(`1st ajax failure: `, url, await result.json());

                        const res = await this.authenticationCheck();

                        if (res.access_token) {

                            this.writeTokensLocalStorage(res);

                            this.overlayStateHandler(false, null);

                            return await fetch(url, {
                                method,
                                headers: {
                                    'authorization': `Bearer ${res.access_token}`,
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(body)
                            });

                        } else {

                            console.log(`2nd ajax failure: `, url, res);

                            this.loginStateHandler(false);
                        }

                    } else {

                        this.overlayStateHandler(false, null);

                        return result;
                    }
                }

                break;

            case `PUT`:

                if (localStorage.getItem('accessToken') == null) {

                    this.loginStateHandler(false);

                } else {

                    result = await fetch(url, {
                        method,
                        headers: {
                            authorization,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(body)
                    });

                    if (result.status === 401) {

                        console.log(`1st ajax failure: `, url, await result.json());

                        const res = await this.authenticationCheck();

                        if (res.access_token) {

                            this.writeTokensLocalStorage(res);

                            this.overlayStateHandler(false, null);

                            return await fetch(url, {
                                method,
                                headers: {
                                    'authorization': `Bearer ${res.access_token}`,
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(body)
                            });

                        } else {

                            console.log(`2nd ajax failure: `, url, res);

                            this.loginStateHandler(false);
                        }

                    } else {

                        this.overlayStateHandler(false, null);

                        return result;
                    }
                }

                break;
        }
    };

    writeTokensLocalStorage = (fetchResponse) => {

        let accessToken = {
            writeTime: Date.now(),
            expiresIn: (fetchResponse.expires_in * 1000),
            accessToken: fetchResponse.access_token,
            ud: btoa(fetchResponse['user-id'])
        };

        localStorage.setItem('accessToken', JSON.stringify(accessToken));
        localStorage.setItem('refreshToken', fetchResponse.refresh_token);
    };

    logoutHandler = async () => {

        let result;

        try {

            result = await this.fetchManager({url: `${logoutUrl}`, method: `DELETE`});

            if (result.ok) {

                this.loginStateHandler(false);

            } else {

                throw new Error(`logoutHander in Index has error`);
            }

        } catch (error) {

            this.loginStateHandler(false);

            if (typeof result !== 'undefined') {

                if (result.status !== 500) {

                    throw new Error(`Something in App is wrong, Check logoutHandler function is RootComponent`);

                } else {

                    throw new Error(`The Logout is failed because of Server Problem`);
                }

            } else {

                this.overlayStateHandler(false, null);

                throw new Error(`Logout Fetch fall in error and the reason is Network`);

            }

        }

    };

    loginStateHandler = (login) => {
        //console.log(`loginStateHandler is called`, this.state);
        if (!login) {

            localStorage.clear();
        }

        this.setState({
            login,
        });

        this.overlayStateHandler(false, null);
    };

    loginHandler = async (username, password) => {

        this.overlayStateHandler(true, this.renderIndeterminateLoader());

        const body = new FormData();
        body.append('username', username);
        body.append('password', password);
        body.append('grant_type', 'password');
        body.append('deviceId', '888');

        let result;

        try {

            result = await fetch(`${loginUrl}`, {
                method: `POST`,
                headers: {
                    'authorization': config.basicAuth,
                },
                body: body,
            });

            this.overlayStateHandler(false, null);

            if (result.ok) {

                const res = await result.json();

                await this.writeTokensLocalStorage(res);

                this.loginStateHandler(true);

                //this.getAuthorities(res);

            } else {

                throw new Error(`loginHander in Index has error`);

            }

        } catch (error) {

            //this.failedLoginHandler(true);

            if (typeof result !== 'undefined') {

                if (result.status === 400) {

                    throw new Error(`Wrong Password`);

                } else if (result.status === 401) {

                    const res = await result.json();

                    throw new Error(`${res.error_description}`);

                } else if (result.status === 500) {

                    throw new Error(`The Server fall in error`);
                } else {

                    throw new Error(`Unknown: `, result.status);
                }

            } else {

                throw new Error(`Login Fetch fall in error and the reason is Network`);

            }

        }
    };

    overlayStateHandler = (overlay, overlayChild) => this.setState({overlay, overlayChild});

    renderOverlay = (overlayChild = this.state.overlayChild) => (
        this.state.overlay && (
            <div className={styles.overlay}>
                {overlayChild}
            </div>
        )
    );

    renderIndeterminateLoader = () => (
        <div className={styles['indeterminate-loader']}/>
    );

    renderSwipeRefreshLoader = () => (
        <div className={styles['swipe-refresh-loader']}>
            <svg className={styles['swipe-refresh-loader__mover']} width="40" height="40" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
                <circle cx="20" cy="20" r="15"/>
            </svg>
        </div>
    );

    render = () => (
        <div className={styles['root-wrapper']}>
            <Helmet
                htmlAttributes={{lang: 'fa', amp: undefined}}
                titleTemplate='اسکن - %s'
                titleAttributes={{itemprop: 'name', lang: 'fa'}}
                meta={[
                    {name: 'description', content: 'صفحه اتصال اعضاء'},
                    {name: 'viewport', content: 'width=device-width, initial-scale=1'},
                ]}
                link={[{rel: 'stylesheet', href: '/dist/styles.css'}]}
            />
            {this.renderOverlay()}
            {
                this.state.login
                    ? (
                        <div className={styles['app-wrapper']}>
                            <div className={styles['app-wrapper__top']}>
                                <Button classes={{root: this.props.classes.exitButton}} onClick={this.logoutHandler}>
                                    خروج
                                </Button>
                            </div>
                            <AppRoutes key='appRoutes' firstRoute={this.firstRoute} fetchManager={this.fetchManager}/>
                        </div>
                    )
                    : (
                        <LoginRoutes loginHandler={this.loginHandler}/>
                    )
            }
        </div>
    );
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(App);
